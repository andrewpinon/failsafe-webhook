# TM Services Webhook

A webook to be used for executing pre-defined commands in our production services without the need to give SSH access to the VM.

## How to use

Hit route specific to action and project.

## Security Considerations and Abuse prevention

- Whitelist IP Address.
- Rate limiting requests.
- Blame logs.

## Maintainability and Codebase Design Choices

TS is good enough JSDoc comments only only on unconventional coding decisions.
@params, @return, @type tags redundant most of the time due to TS, but some complex/hard to understand functions could still benefit from additional exlenations regarding their parameters and return type.

@examples - Useful though for tooltips in vscode
