import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import webhookRoutes from './src/routes/webhooks';
import routes from './src/routes/index';
import cors from 'cors';
import logAction from './src/middleware/logAction';
import verifyJWTExcludingLogin from './src/middleware/verifyJWT';
import config from './src/config/config';
import rateLimit from './src/middleware/rateLimit';
import initRateMemoryStore from './src/utils/initialize/initRateLimit';
import { IRateStore } from './types';
import initDBConnection from './src/utils/initialize/initDBConnection';

const { PORT } = config;

initDBConnection();
const rateLimitMemoryStore: IRateStore = initRateMemoryStore();

const app = express();
const allowList = ['http://localhost:3000', process.env.DEV1_IP as string];

app.use(
  cors({
    origin: allowList,
    credentials: true,
  }),
);
app.use(bodyParser.json());
app.use(cookieParser());

/**
 * rateLimit is setup like this to avoid multiple instances of the rateLimitMemoryStore
 */
app.use(rateLimit(rateLimitMemoryStore));
app.use(verifyJWTExcludingLogin);
app.use(logAction);

app.use('/hook', ...webhookRoutes);
app.use('/', ...routes);

//so ports wont clash when doing unit tests
if (process.env.NODE_ENV !== 'test') {
  app.listen(PORT, () => {
    console.log(`Server is running on port http://localhost:${PORT}`);
  });
}

export default app;
