import dotenv from 'dotenv';
dotenv.config();

const config = {
  PORT: process.env.PORT || 2700,
  JWT_SECRET: process.env.JWT_SECRET || 'uMu$tChAngeME',
  MAX_REQUEST_LIMIT: Number(process.env.MAX_REQUEST_LIMIT) || 30,

  AZURE_CLIENT_SECRET: process.env.AZURE_CLIENT_SECRET || '',
  AZURE_CLIENT_ID: process.env.AZURE_CLIENT_ID || '',

  SSH_ADDRESS_DEV: process.env.SSH_ADDRESS_DEV || 'Change me to your local ssh command',

  CHATEASY_SERVER_PATH: process.env.CHATEASY_SERVER_PATH || '/var/web/tm-chateasy-server',
  CHATEASY_CLIENT_PATH: process.env.CHATEASY_CLIENT_PATH || 'var/web/tm-chateasy-client',
  CHATEASY_PM2_ID: process.env.CHATEASY_PM2_ID || '30',
  CHATEASY_AZURE_SUBSCRIPTION: process.env.CHATEASY_CLIENT_PATH || '',
  CHATEASY_AZURE_RESOURCE_GROUP: process.env.CHATEASY_AZURE_RESOURCE_GROUP || '',
  CHATEASY_SERVER_1_VM_NAME: process.env.CHATEASY_SERVER_1_VM_NAME || '',
  CHATEASY_SERVER_2_VM_NAME: process.env.CHATEASY_SERVER_2_VM_NAME || '',
  CHATEASY_SERVER_3_VM_NAME: process.env.CHATEASY_SERVER_3_VM_NAME || '',
  CHATEASY_CLIENT_1_VM_NAME: process.env.CHATEASY_SERVER_3_VM_NAME || '',

  RABBITMQ_1_VM_NAME: process.env.RABBITMQ_1_VM_NAME || '',

  //DevDashboard
  DEV_DASHBOARD_SUBSCRIPTION: process.env.DEV_DASHBOARD_SUBSCRIPTION || '',
  DEV_DASHBOARD_RESOURCE_GROUP: process.env.DEV_DASHBOARD_RESOURCE_GROUP || '',
  DEV_DASHBOARD_VM_NAME: process.env.DEV_DASHBOARD_VM_NAME || '',

  MONGO_URI: process.env.MONGODB_URI || 'mongodb://localhost:27017/tm-admin',
};
export default config;
