import { Request, Response } from 'express';

function charteasyRestart(req: Request, res: Response) {
  res.send('Placeholder Restart ChartEasy PM2!');
}

export default charteasyRestart;
