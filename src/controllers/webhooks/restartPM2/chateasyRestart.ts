import { Request, Response } from 'express';
import logger from '../../../utils/helpers/logger';
import RemoteCodeExecute from '../../../core/RemoteCodeExecution/RemoteCodeExecution';
import { DefaultResponse } from '../../../../types';

/**
 * @description This function restarts the chateasy server on the dev server.
 * This is the command you run on your local machine's terminal. i.e. ssh intlo VM pm2 restart PID etc.
 * TEMP: this just restarts the chateasy server on dev for now
 *
 * source ~/.nvm/nvm.sh needed to run npm commands on the server
 */
async function chateasyRestart(_: Request, res: Response) {
  const commands: string[] = ['source ~/.nvm/nvm.sh npm run build', 'pm2 restart 31'];
  const sshKey = '~/.ssh/tm-andrew';
  const sshAddress = 'james@206.189.168.89';
  const path = '/mnt/volume_sfo2_01/var/web/tm-chateasy-client';

  try {
    const restartChatEasy = new RemoteCodeExecute(sshKey, sshAddress, commands, path);
    const response: DefaultResponse<string> = await restartChatEasy.execute();
    res.status(200).send(response);
  } catch (err) {
    logger.error(err);
    return res.status(500).send({ error: err });
  }
}

export default chateasyRestart;
