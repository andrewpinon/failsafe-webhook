import express from 'express';
import tmAdminRestart from './tmAdminRestart';
import chateasyRestart from './chateasyRestart';
import charteasyRestart from './charteasyRestart';
import restartTestingRoutePM2 from './restartTestPM2';

const router = express.Router();

router.get('/restart-admin-pm', tmAdminRestart);
router.get('/restart-chateasy-pm', chateasyRestart);
router.get('/restart-charteasy-pm', charteasyRestart);
router.get('/restart-test-pm', restartTestingRoutePM2);

export default router;
