import { Request, Response } from 'express';

function restartTestingRoutePM2(_: Request, res: Response) {
  res.status(200).send({ data: 'Test Route Hit Successfuly' });
}
export default restartTestingRoutePM2;
