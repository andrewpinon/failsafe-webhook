import { Request, Response } from 'express';

function tmAdminRestart(req: Request, res: Response) {
  res.send('Restart TM Admin!');
}

export default tmAdminRestart;
