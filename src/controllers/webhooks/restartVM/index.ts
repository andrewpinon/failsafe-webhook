import express from 'express';
import restartChartEasyVM from './restartChartEasyVM';
import restartSelfVM from './restartSelfVM';
import restartChatEasyVM from './restartChatEasyVM';
import restartRabbitmqVM from './restartRabbitmqVM';

const router = express.Router();

// router.get('/restart-admin-vm', tmAdminRestart);
router.get('/restart-chateasy-vm', restartChatEasyVM);
router.get('/restart-charteasy-vm', restartChartEasyVM);
router.get('/restart-self-vm', restartSelfVM);
router.get('/restart-rabbitmq-vm', restartRabbitmqVM);

export default router;
