import { Request, Response } from 'express';
import { DefaultResponse } from '../../../../types';

function restartChartEasyVM(req: Request, res: Response<DefaultResponse<string>>) {
  console.log('Placeholder restart charteasy VM');
  res.status(200).send({ data: 'Restart ChartEasy VM!' });
}

export default restartChartEasyVM;
