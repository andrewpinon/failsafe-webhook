import { Request, Response } from 'express';
import config from '../../../config/config';
import AzureCommand from '../../../core/AzureCommands/AzureCommand';

async function restartChatEasyVM(_: Request, res: Response) {
  const chatEasyVMNames = [
    config.CHATEASY_SERVER_3_VM_NAME,
    config.CHATEASY_SERVER_2_VM_NAME,
    config.CHATEASY_SERVER_1_VM_NAME,
    config.CHATEASY_CLIENT_1_VM_NAME,
  ];

  const azureExecuter = new AzureCommand(
    config.CHATEASY_AZURE_SUBSCRIPTION,
    config.CHATEASY_AZURE_RESOURCE_GROUP,
    chatEasyVMNames,
  );

  try {
    const response = await azureExecuter.restartVM();
    res.status(200).send({ data: response });
  } catch (error) {
    res.status(500).send({ data: null, error: error });
  }
}

export default restartChatEasyVM;
