import { Request, Response } from 'express';
import config from '../../../config/config';
import AzureCommand from '../../../core/AzureCommands/AzureCommand';

async function restartRabbitmqVM(_: Request, res: Response) {
  const rabbitmqVMNames = [config.RABBITMQ_1_VM_NAME];

  const azureExecuter = new AzureCommand(
    config.CHATEASY_AZURE_SUBSCRIPTION,
    config.CHATEASY_AZURE_RESOURCE_GROUP,
    rabbitmqVMNames,
  );

  try {
    const response = await azureExecuter.restartVM();
    res.status(200).send({ data: response });
  } catch (error) {
    res.status(500).send({ data: null, error: error });
  }
}

export default restartRabbitmqVM;
