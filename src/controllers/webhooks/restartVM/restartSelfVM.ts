import { Request, Response } from 'express';
import AzureCommand from '../../../core/AzureCommands/AzureCommand';
import logger from '../../../utils/helpers/logger';
import { DefaultResponse } from '../../../../types';
import config from '../../../config/config';

async function restartSelfVM(_: Request, res: Response<DefaultResponse<string>>) {
  console.log("restarting self")
  const devDashboardVMs = [config.DEV_DASHBOARD_VM_NAME];
  const restartSelf = new AzureCommand(
    config.DEV_DASHBOARD_RESOURCE_GROUP,
    config.DEV_DASHBOARD_SUBSCRIPTION,
    devDashboardVMs,
  );

  try {
    // const results = await restartSelf.startVM();
    const results = await restartSelf.restartVM();
    return res.status(200).send({ data: results });
  } catch (error: any) {
    console.log(error)
    logger.error(error);
    return res.status(500).send({ data: null, error: error });
  }
}

export default restartSelfVM;
