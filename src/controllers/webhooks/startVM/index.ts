import express from 'express';
import startSelfVM from './startSelfVM';
import startChatEasyVM from './startChatEasyVM';

const router = express.Router();

router.get('/start-self-vm', startSelfVM);
router.get('/start-self-vm', startChatEasyVM);

export default router;
