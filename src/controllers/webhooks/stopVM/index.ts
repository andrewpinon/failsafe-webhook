import express from 'express';
import stopSelfVM from './stopSelfVM';

const router = express.Router();

router.get('/stop-self-vm', stopSelfVM);

export default router;
