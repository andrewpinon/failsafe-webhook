import logger from '../../utils/helpers/logger';
import getAzureAuthToken from '../../utils/helpers/obtainAzureAuthToken';

enum modes {
  restart = 'restart',
  start = 'start',
  stop = 'stop',
}

class AzureCommand {
  private subscription: string;
  private resourceGroup: string;
  private virtualMachineNames: string[];
  private baseUrl: string;

  constructor(subscription: string, resourceGroup: string, virtualMachineNames: string[]) {
    this.subscription = subscription;
    this.resourceGroup = resourceGroup;
    this.virtualMachineNames = virtualMachineNames;

    this.baseUrl = `https://management.azure.com/subscriptions/${this.subscription}/resourceGroups/${this.resourceGroup}/providers/Microsoft.Compute/virtualMachines`;
  }

  private tranformUrl(vmName: string, mode: modes) {
    const azureApiModes: { [key: string]: string } = {
      restart: 'restart?api-version=2024-03-01',
      start: 'start?api-version=2024-03-01',
      stop: 'powerOff?api-version=2024-03-01',
    };

    return `${this.baseUrl}/${vmName}/${azureApiModes[mode]}`;
  }

  private async executeFetch(
    AZURE_VM_RESTART_URL: string,
    requestOptions: RequestInit,
  ): Promise<number | string> {
    /**
     *Azure endpoint only returns status code 202 when succesful. No text or json
     */
    const response = await fetch(AZURE_VM_RESTART_URL, requestOptions);

    if (!response.ok) {
      const errorResult = await response.json();
      throw 'Something went wrong: ' + errorResult.error.message;
    }

    return 'Successfuly Triggered VM Restart';
  }

  private async azureAuthenticate() {
    const authToken: string = await getAzureAuthToken();
    const headers = new Headers();
    headers.append('Authorization', `Bearer ${authToken}`);
    const requestOptions = {
      method: 'POST',
      headers: headers,
    };

    return requestOptions;
  }

  async restartVM(): Promise<string> {
    const requestOptions = await this.azureAuthenticate();

    try {
      const final = await Promise.all(
        this.virtualMachineNames.map((vm) =>
          this.executeFetch(this.tranformUrl(vm, modes.restart), requestOptions),
        ),
      );
      return final.toString();
    } catch (error: any) {
      throw error;
    }
  }

  async startVM(): Promise<string> {
    const requestOptions = await this.azureAuthenticate();

    try {
      const final = await Promise.all(
        this.virtualMachineNames.map((vm) =>
          this.executeFetch(this.tranformUrl(vm, modes.start), requestOptions),
        ),
      );
      return final.toString();
    } catch (error: any) {
      throw error;
    }
  }

  async stopVM(): Promise<string> {
    const requestOptions = await this.azureAuthenticate();

    try {
      const final = await Promise.all(
        this.virtualMachineNames.map((vm) =>
          this.executeFetch(this.tranformUrl(vm, modes.stop), requestOptions),
        ),
      );
      return final.toString();
    } catch (error: any) {
      throw error;
    }
  }
}

export default AzureCommand;
