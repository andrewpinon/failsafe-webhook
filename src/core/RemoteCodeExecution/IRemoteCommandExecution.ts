interface IRemoveCommandExecution {
  sshKey: string;
  sshAddress: string;
  path?: string;

  execute(): void;
}

export default IRemoveCommandExecution;
