import IRemoveCommandExecution from './IRemoteCommandExecution';
import { DefaultResponse } from '../../../types';
import { exec } from 'child_process';
import { promisify } from 'util';
import paraseCommands from '../../utils/helpers/parseCommands';

class RemoteCodeExecute implements IRemoveCommandExecution {
  sshKey: string;
  sshAddress: string;
  commands: string[];
  path?: string;

  constructor(sshKey: string, sshAddress: string, commands: string[], path?: string) {
    this.sshKey = sshKey;
    this.sshAddress = sshAddress;
    this.commands = commands;

    if (path) {
      this.path = path;
    }
  }

  async execute(): Promise<DefaultResponse<string>> {
    const asyncExec = promisify(exec);
    const pCommands = paraseCommands(this.commands);
    const finalCommand = `ssh -i ${this.sshKey} ${this.sshAddress} '
    cd ${this.path && this.path}
    ${pCommands}'
    `;

    const responseObj: DefaultResponse<string> = {
      data: null,
    };

    try {
      const response = await asyncExec(finalCommand);

      if (response.stderr) {
        throw new Error(response.stderr);
      }

      responseObj.data = response.stdout;
      return responseObj;
    } catch (err: any) {
      throw new Error(err.stderr as string);
    }
  }
}

export default RemoteCodeExecute;
