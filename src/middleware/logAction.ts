import { NextFunction, Request, Response } from "express";
import logger from "../utils/helpers/logger";
/**
 * Will Log action details and user.
 */
function logAction(req: Request, res: Response, next: NextFunction) {
  const user = req.body.user;

  const logObj = {
    endpoint: req.url,
    accountId: user ? user.aid : "Unauthenticated User",
    origin: req.ip,
  };

  logger.info(`${JSON.stringify(logObj)}`);
  next();
}

export default logAction;
