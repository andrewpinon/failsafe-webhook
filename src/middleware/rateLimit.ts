import { NextFunction, Request, Response } from 'express';
import { IRateStore } from '../../types';
import logger from '../utils/helpers/logger';

/**
 * @param rateStore An instance of the RateStore class. Used in storing and managing http requests and rate limiting information .
 * See {@link ../utils/rateStore.ts}
 */
function rateLimit(rateStore: IRateStore) {
  return function (_: Request, res: Response, next: NextFunction) {
    const currentTime: number = Date.now();

    if (!rateStore.addRequest(currentTime)) {
      logger.error({ error: 'too many requests' });
      return res.status(429).send({ error: 'too many requests' });
    }
    next();
  };
}

export default rateLimit;
