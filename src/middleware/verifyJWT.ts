import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import config from '../config/config';
import { expireToken } from '../routes/authentication/logout';

function verityJWT(req: Request, res: Response, next: NextFunction) {
  const token = req.cookies.token;

  if (token === undefined)
    return res.status(401).send({ error: 'Unauthorized Access' });

  /**
   * JWT_SECRET - This should be the same secret used to sign the JWT in TM-Admin-Server
   */
  let user = null;

  try {
    user = jwt.verify(token, config.JWT_SECRET);
  } catch (err) {
    return expireToken(res);
  }
  // store it in the request object for later use
  req.body.user = user;
  if (!user) return res.status(500).send({ error: 'Invalid Token' });
  next();
}

// This function will allow access to the login route without authentication
function verifyJWTExcludingLogin(
  req: Request,
  res: Response,
  next: NextFunction,
) {
  if (req.url === '/auth/login') return next();

  verityJWT(req, res, next);
}

export default verifyJWTExcludingLogin;
