import mongoose, { Schema } from 'mongoose';

export interface Account {
  email: string;
  password: string;
  name: string;
}

const AccountSchema = new Schema<Account>({
  email: String,
  password: String,
  name: String,
});

export const MAccount = mongoose.model('Account', AccountSchema, 'User');
