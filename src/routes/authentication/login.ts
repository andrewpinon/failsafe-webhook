import { Router } from 'express';
import { MAccount, Account } from '../../models/Account';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import config from '../../config/config';
import { loginLogger } from '../../utils/helpers/logger';
const router = Router();

router.post('/auth/login', async (req, res) => {
  const { email, password } = req.body;

  const user: Account | null = await MAccount.findOne({
    email,
    isActive: true,
  }).lean();
  if (!user) return res.status(401).send({ error: 'authentication failed' });

  const validAcc = await bcrypt.compare(password, user.password);
  if (!validAcc) return res.status(401).send({ error: 'authentication failed' });

  const jwtToken = jwt.sign(user, config.JWT_SECRET, { expiresIn: '1H' });
  const options = {
    maxAge: 1000 * 60 * 60,
    httpOnly: true, // Cookie will not be exposed to client side code
  };
  res.cookie('token', jwtToken, options);
  loginLogger.info({ loginAttempt: email, success: true });

  console.log("AHHH")
  return res.status(200).send({ status: true });
});

export default router;
