import { Request, Response, Router } from 'express';
const router = Router();

export function expireToken(res: Response) {
  res.cookie('token', '', { expires: new Date(0) });
  res.status(401).send({ error: 'Expired Token' });
}

router.get('/auth/logout', (req: Request, res: Response) => {
  expireToken(res);
});

export default router;
