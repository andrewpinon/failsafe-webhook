import login from './authentication/login';
import logout from './authentication/logout';

export default [login, logout];
