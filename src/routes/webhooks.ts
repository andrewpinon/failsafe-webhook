import restartPM2 from '../controllers/webhooks/restartPM2';
import restartVM from '../controllers/webhooks/restartVM';
import startVM from '../controllers/webhooks/startVM';
import stopVM from '../controllers/webhooks/stopVM';

/**
 * Don't import this in the index of routes
 * routes here are prefixed with /hook and is used separetely at server.ts
 */
export default [restartPM2, restartVM, startVM, stopVM];
