import winston from 'winston';

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.combine(
    winston.format.timestamp({
      format: 'YYYY-MMM-DD HH:mm:ss',
    }),
    winston.format.prettyPrint(),
  ),
  transports: [
    // - Write all logs with importance level of `error` or less to `error.log`
    // - Write all logs with importance level of `info` or less to `combined.log`
    // - maxsize is in bytes. maxFiles delete file if it reaches more than 4
    new winston.transports.File({
      filename: 'logs/error.log',
      level: 'error',
      maxsize: 5000000,
      maxFiles: 4,
    }),
    new winston.transports.File({ filename: 'logs/combined.log', maxsize: 5000000, maxFiles: 4 }),
  ],
});

export const loginLogger = winston.createLogger({
  level: 'info',
  format: winston.format.combine(
    winston.format.timestamp({
      format: 'YYYY-MMM-DD HH:mm:ss',
    }),
    winston.format.prettyPrint(),
  ),
  transports: [new winston.transports.File({ filename: 'logs/login.log', level: 'info' })],
});

// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
if (process.env.NODE_ENV !== 'production') {
  console.log('NOT IN PRODUCTION');
  logger.add(
    new winston.transports.Console({
      format: winston.format.simple(),
    }),
  );
}

export default logger;
