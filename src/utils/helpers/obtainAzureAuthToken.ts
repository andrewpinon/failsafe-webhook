import config from '../../config/config';
import logger from './logger';

const MICROSOFT_OATH_URL =
  'https://login.microsoftonline.com/50731035-adc7-475a-956a-0bd5ce790e40/oauth2/v2.0/token';

async function getAzureAuthToken() {
  try {
    const myHeaders = new Headers();
    const urlencoded = new URLSearchParams();

    myHeaders.append('Content-Type', 'application/x-www-form-urlencoded');
    urlencoded.append('grant_type', 'client_credentials');
    urlencoded.append('client_secret', config.AZURE_CLIENT_SECRET);
    urlencoded.append('client_id', config.AZURE_CLIENT_ID);
    urlencoded.append('scope', 'https://management.azure.com/.default');

    const requestOptions: Object = {
      method: 'POST',
      headers: myHeaders,
      body: urlencoded,
      redirect: 'follow',
    };

    const response = await fetch(MICROSOFT_OATH_URL, requestOptions);
    /**
     * token_type: 'Bearer',
     * expires_in: number,
     * ext_expires_in: number,
     * access_token: string
     */
    const parsedResponse = await response.json();
    return parsedResponse.access_token;
  } catch (err) {
    logger.error(err);
    throw new Error(err as string);
  }
}

export default getAzureAuthToken;
