/**
 * RateStore class is a in memory store to keep track of the rate limiting information.
 * making sure only one instance is created.
 * @class RateStore
 */
import { IRateStore } from '../../../types';

export default class RateStore implements IRateStore {
  private requestTimeLog: number[];
  private requests: number;
  private intervalId: NodeJS.Timeout | null;
  private maxRequests: number;
  private rateTimeLimit: number;

  constructor(rateTimeLimit: number, maxRequests: number) {
    this.requests = 0;
    this.requestTimeLog = [];
    this.maxRequests = maxRequests;
    this.rateTimeLimit = rateTimeLimit;
    this.intervalId = null;

    if (this.intervalId) clearInterval(this.intervalId);

    this.intervalId = setInterval(() => {
      this.removeRequestCount();
    }, rateTimeLimit);
  }

  addRequest(currentTime: number): boolean {
    /**
     * Checking to see if request is older than rate limit time
     * This is because removeRequestCount is called every rateTimeLimit but it needs 2 cycles to remove requests older than rateTimeLimit
     * This is because some request might get triggered in the middle of the cycle and will only be removed in the next cycle.
     */
    if (currentTime - this.requestTimeLog[0] > this.rateTimeLimit) {
      this.requestTimeLog.shift();
      this.requests--;
    }

    if (this.requests >= this.maxRequests) return false;

    this.requests++;
    this.requestTimeLog.push(Date.now());
    return true;
  }

  removeRequestCount(): void {
    if (this.requestTimeLog.length === 0) return;

    const currentTime: number = Date.now();
    const newTimeLogs = this.requestTimeLog.filter(
      (requestDate) => currentTime - requestDate > this.rateTimeLimit,
    );

    this.requestTimeLog = newTimeLogs;
    this.requests = newTimeLogs.length;
  }
}
