import mongoose from 'mongoose';
import config from '../../config/config';
import logger from '../helpers/logger';

export default function initDBConnection() {
  mongoose
    .connect(config.MONGO_URI)
    .finally(() => {
      console.log('Connected to DB');
    })
    .catch((err) => logger.error(err));
}

