import { IRateStore } from '../../../types';
import config from '../../config/config';
import RateStore from '../helpers/rateStore';

export default function initRateMemoryStore(): IRateStore {
  /**
   * 5 minutes before the user's requests counter is reset
   * @example 1000 * 60 * 5 = 300000 ms = 5 minutes
   */
  const RATE_LIMIT_TIME: number = 1000 * 60 * 2;
  const MAX_REQUESTS: number = config.MAX_REQUEST_LIMIT;

  /**
   * Preference init here to avoid cluttering server.js
   * Creating a new instance of RateStore class
   */
  const instance: IRateStore = new RateStore(RATE_LIMIT_TIME, MAX_REQUESTS);
  return instance;
}
