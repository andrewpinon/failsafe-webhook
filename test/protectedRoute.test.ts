import supertest from 'supertest';
import app from '../server';
import mongoose from 'mongoose';

afterAll(async () => {
  await mongoose.connection.close();
});

describe('Protected Routes', () => {
  it('should return status 401 Unauthorized Access', async () => {
    await supertest(app).get('/hook/restart-chateasy-pm').expect(401);
  });
});
