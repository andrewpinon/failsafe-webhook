import supertest from 'supertest';
import app from '../server';
import config from '../src/config/config';
import mongoose from 'mongoose';

afterAll(async () => {
  await mongoose.connection.close();
});

describe('rate limit', () => {
  it('should return', async () => {
    const requests = [];
    console.log(config.MAX_REQUEST_LIMIT);
    for (let i = 0; i < config.MAX_REQUEST_LIMIT + 1; i++) {
      requests.push(supertest(app).get('/hook/restart-chateasy-pm'));
    }

    const responses = await Promise.all(requests);
    expect(responses[config.MAX_REQUEST_LIMIT].status).toBe(429);
  });
});
