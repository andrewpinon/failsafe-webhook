export interface IRateStore {
  addRequest(currentTime: number): boolean;
  removeRequestCount(): void;
}

export interface User {
  aid: string;
}

export interface DefaultResponse<T> {
  data: t | null;
  error?: string;
}

export interface AzureError {
  error: {
    code: string;
    message: string;
  };
}
